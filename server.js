require('dotenv').config()

const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLInputObjectType
} = require('graphql');
const mongoose = require('mongoose');
const cors = require ('cors');
const app = express();

// Connect to MongoDB Atlas
mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to Database'));

 // Enable CORS
app.use(cors());

// Define Mongoose models
const paragraphSchema = new mongoose.Schema({
    chapitre: { type: String, required: true },
    titre: { type: String, required: true },
    theorie: { type: String, required: false },
    technique: { type: String, required: false },
    citations: { type: [String], required: false },
    commentaire: { type: [String], required: false },
    texte: [{
        id: { type: String, required: true },
        contenu: { type: String, required: true }
    }]
});

// Static method to update paragraph by its ID
paragraphSchema.statics.updateParagraph = async function(paragraphId, newChapterName) {
  try {
      const updatedParagraph = await this.findByIdAndUpdate(
          paragraphId,
          { chapitre: newChapterName },
          { new: true }
      );
      return updatedParagraph;
  } catch (error) {
      console.error('Error while updating paragraph:', error.message);
      throw error;
  }
};

const chapterSchema = new mongoose.Schema({
    titre: { type: String, required: true },
    numeroChapitre: { type: Number, required: true },
    resume: { type: String, required: true },
    paragraphes: [{
        numeroParagraphe: { type: Number, required: true },
        titreParagraphe: { type: String, required: true }
    }],
    tag: { type: String, required: false }
});

const Paragraph = mongoose.model('Paragraph', paragraphSchema);
const Chapter = mongoose.model('Chapter', chapterSchema);

// Define GraphQL types
//When we create fields based on custom object types, we are connecting two objects (in graph theory terminology, we are creating an edge).
const TexteType = new GraphQLObjectType({
    name: 'Texte',
    fields: {
        id: { type: GraphQLNonNull(GraphQLString) },
        contenu: { type: GraphQLNonNull(GraphQLString) }
    }
});

const ParagraphType = new GraphQLObjectType({
    name: 'Paragraph',
    fields: {
        id: { type: GraphQLString },
        chapitre: { type: GraphQLNonNull(GraphQLString) },
        titre: { type: GraphQLNonNull(GraphQLString) },
        theorie: { type: GraphQLString },
        technique: { type: GraphQLString },
        citations: { type: GraphQLList(GraphQLString) },
        commentaire: { type: GraphQLList(GraphQLString) },
        texte: { type: GraphQLList(TexteType) }
    }
});

const ParagrapheType = new GraphQLObjectType({
    name: 'Paragraphe',
    fields: {
        numeroParagraphe: { type: GraphQLNonNull(GraphQLInt) },
        titreParagraphe: { type: GraphQLNonNull(GraphQLString) }
    }
});

const ChapterType = new GraphQLObjectType({
    name: 'Chapter',
    fields: {
        id: { type: GraphQLString },
        titre: { type: GraphQLNonNull(GraphQLString) },
        numeroChapitre: { type: GraphQLNonNull(GraphQLInt) },
        resume: { type: GraphQLNonNull(GraphQLString) },
        paragraphes: { type: GraphQLList(ParagrapheType) },
        tag: { type: GraphQLString }
    }
});

// Define GraphQL input types
const TexteInputType = new GraphQLInputObjectType({
    name: 'TexteInput',
    fields: {
        id: { type: GraphQLNonNull(GraphQLString) },
        contenu: { type: GraphQLNonNull(GraphQLString) }
    }
});

const ParagraphInputType = new GraphQLInputObjectType({
    name: 'ParagraphInput',
    fields: {
        chapitre: { type: GraphQLNonNull(GraphQLString) },
        titre: { type: GraphQLNonNull(GraphQLString) },
        theorie: { type: GraphQLString },
        technique: { type: GraphQLString },
        citations: { type: GraphQLList(GraphQLString) },
        commentaire: { type: GraphQLList(GraphQLString) },
        texte: { type: GraphQLList(TexteInputType) }
    }
});

// Define GraphQL queries
const RootQueryType = new GraphQLObjectType({
    name: 'Query',
    fields: {
        paragraphes: {
            type: new GraphQLList(ParagraphType),
            args: {
              theorie: {type: GraphQLString},
              technique: {type: GraphQLString},
              citations: {type: GraphQLString}
            },
            resolve: (parent, args) => {
              const filter = {};
              if (args.theorie) {
                  filter.theorie = args.theorie;
              }
              if (args.technique) {
                  filter.technique = args.technique;
              }
              return Paragraph.find(filter);
          }
      },
        chapters: {
            type: new GraphQLList(ChapterType),
            resolve: () => Chapter.find()
        }
    }
});

// Define GraphQL mutations
const RootMutationType = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addParagraph: {
            type: ParagraphType,
            args: {
                chapitre: { type: GraphQLNonNull(GraphQLString) },
                titre: { type: GraphQLNonNull(GraphQLString) },
                theorie: { type: GraphQLString },
                technique: { type: GraphQLString },
                citations: { type: GraphQLList(GraphQLString) },
                commentaire: { type: GraphQLList(GraphQLString) },
                texte: { type: GraphQLList(TexteInputType) }
            },
            resolve: (parent, args) => {
                const newParagraph = new Paragraph(args);
                return newParagraph.save();
            }
        },
        addChapter: {
            type: ChapterType,
            description: "Schema pour chaque chapitre de ma thèse",
            args: {
                titre: { type: GraphQLNonNull(GraphQLString) },
                numeroChapitre: { type: GraphQLNonNull(GraphQLInt) },
                resume: { type: GraphQLNonNull(GraphQLString) },
                paragraphes: { type: GraphQLList(new GraphQLInputObjectType({
                    name: 'ParagrapheInput',
                    fields: {
                        numeroParagraphe: { type: GraphQLNonNull(GraphQLInt) },
                        titreParagraphe: { type: GraphQLNonNull(GraphQLString) }
                    }
                })) },
                tag: { type: GraphQLString }
            },
            resolve: (parent, args) => {
                const newChapter = new Chapter(args);
                return newChapter.save();
            }
        },
        updateParagraph: {
          type: ParagraphType,
          args: {
              paragraphId: { type: GraphQLNonNull(GraphQLString) },
              newChapterName: { type: GraphQLNonNull(GraphQLString) }
          },
          resolve: async (_, { paragraphId, newChapterName }) => {
            try {
                const updatedParagraph = await Paragraph.updateParagraph(paragraphId, newChapterName);
                return updatedParagraph;
            } catch (error) {
              throw new Error(`Error while updating paragraph: ${error.message}`);
                }
            }
          },
          deleteParagraph: {
            type: ParagraphType,
            args: {
                paragraphId: { type: GraphQLNonNull(GraphQLString) },
                chapterId: { type: GraphQLString },
            },
            resolve: async (_, { paragraphId }) => {
                try {
                    const deletedParagraph = await Paragraph.findByIdAndDelete(paragraphId);
                    return deletedParagraph;
                } catch (error) {
                    throw new Error(`Error while deleting paragraph: ${error.message}`);
                }
              }
            }, 
          addComment: {
            type: ParagraphType,
            args: {
              paragraphId: { type: GraphQLNonNull(GraphQLString) },
              commentaire: { type: GraphQLList(GraphQLString) }
            },
            resolve: async (_, { paragraphId, commentaire }) => {
              try {
                  const updatedParagraph = await Paragraph.findByIdAndUpdate(
                      paragraphId,
                      { $push: { commentaire: commentaire } },
                      { new: true }
                  );
                  return updatedParagraph;
              } catch (error) {
                  throw new Error(`Error while adding comment: ${error.message}`);
              }
            }
          }
        } 
      })

// Create schema
const schema = new GraphQLSchema({
    query: RootQueryType,
    mutation: RootMutationType
});

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true
}));

app.listen(5000, () => console.log('Server Running'));
